These are not linear, and there shouldn't be any order. Many "categories"
overlap - tags would be better in the future.

## Data notebooks

TW5 will likely be what I write on and store notes into. The architecture is
great: microkernel, plugins, single page app, JS...

- TiddlyWiki (TW5). Yes.
- Observable.js https://beta.observablehq.com/. Yes.
- Jupyter. Python; not applicable to me; useful for ideas, though.

## Organization

- Wallabag https://github.com/wallabag/wallabag. Saves websites to classify
  articles. Similar to what I'm doing _right now_.

## Data visualization

- Cubism chart library by Square. https://square.github.io/cubism/
- Tag based filesystems (WinFS)
    - Git Annex tag view
    - Symlink trees. NixOS/Exherbo/Gobo are great OS-level examples.
- D3
    - Blocks https://bl.ocks.org/

## Filesystems

- LoggedFS. FUSE layer that logs all file operations (read/write/etc)

## Site building

- GrapeDrop https://grapedrop.com/. Responsize open source drag/drop website
  builder. Also see GrapesJS, the underlying framework.

## CSS

- Reference http://cssreference.io/
- Comparision of modular CSS practices, https://spaceninja.com/2018/09/17/what-is-modular-css/
- Tailwind CSS. Utility-oriented CSS leveraging PostCSS. Good for HTML-in-JS and
  removes the need to have mutliple CSS files per component with arbitrary
  classnames. Looks healthier. Needs more research.
- PurgeCSS. Remove unused CSS classes. Useful for Tailwind CSS.
- Houdini https://lab.iamvdo.me/houdini/

## JS

This list could be the longest once I consolidate everything.

- Idle Until Urgent. Queue work to be done when the browser is idle, or, when
  it's absolutely needed. Transparent, similar to Proxies.
    - https://philipwalton.com/articles/idle-until-urgent/
    - https://github.com/GoogleChromeLabs/idlize/tree/master/lib
- Mutation Observer https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver.
  Useful for lazy-loading images only when a user has scrolled them into view
- Gulp https://buddy.works/blog/webpack-vs-gulp. To stop forcing Webpack to do
  what it isn't designed for.
- Chainable mock object to avoid null references https://github.com/slmgc/Nothing
- WASM https://webassembly.studio/
- Gia https://github.com/giantcz/gia. A React-inspired framework for updating
  DOM components that's only 2kb. Has no reviver; uses refs instead. Good for
  server rendered HTML, but that's about it. Useful for taking voko to more than
  a reviver.

### State

- Redux (also an example with Nylas Mail: https://www.nylas.com/blog/structuring-a-complex-react-redux-project)
- Unstated https://github.com/jamiebuilds/unstated
- Laco state management https://github.com/deamme/laco/tree/master/packages/laco
  similar to Unstated and Redux but does a nice job of seperating state from
  components. There's no Provider component with `this.setState()`. There's a
  `Store` class which is a good seperation of concerns.
- Observables. See RxJS
  
## Package management

- Spack
- Medium article on "So you want to write a package manager?"
- FOSDEM 2018 room on Packer Management
- Universal URLs for packages https://github.com/package-url/purl-spec
- Soft talk on package management becoming known https://archive.fosdem.org/2018/schedule/event/packagemangementunites/
- Tink https://github.com/npm/tink. Remove node_modules/ for a project by
  instead creating symlink trees to...? TODO. Been a while. Not ready.

## Interactive programming

Aka why is Quokka.js and webpack-dev-server the closest things to conversational
programming in 2018? Why throw out all of the system state when hot reloading?

Referred to as "Reactive", "Conversation", or "Interactive" programming, but not
as"Live" programming, which is for on-the-fly music/art production.

https://en.wikipedia.org/wiki/Interactive_programming

### Languages

- SmallTalk. Everyone mentions this...needs more research. If it's true then
  learn it.
- Skip (also called Reflex) by Facebook. Unmaintained. The idea was to have the
  compiler understand what areas of code could be safely memoized. It seems as
  if all operations were cached.
    - Video: https://www.youtube.com/embed/AGkSHE15BSs

### Tools

- Quokka.js. Re execute JS every key stroke and log console statements inline
  with the code. Great for testing algorithms. Bad for async tasks like I/O
  since it spams the endpoint (FS/HTTP/etc).
- Observable.js https://beta.observablehq.com/@mbostock/five-minute-introduction
- rr by Mozilla. C/C++ debugging for Linux that _records_ a failure, and then
  you deterministally debug the _replay_ as many times as needed. Supports
  reverse execution with gdb. https://rr-project.org/

### Network calls

- Node Replay http://documentup.com/assaf/node-replay. Fake HTTP responses to
  help API development. Great for not spamming a real server during Quokka-like
  hot reloading of code.

## Projects

TODO

## Mail

### Clients

- Cypht https://cypht.org/. Multi-account and lightweight. PHP and JS. Plugins
  for calendars and news readers. One person.
- RainLoop http://rainloop.net/. Knockout JS application. Active. Has a nice UI
  that's not modern/material.
- WebMail Lite 8 by Afterlogic. DIY architecture. Web application written in
  shell scripts and PHP. JS frontend.
- MailPile. Python backend-only mail client. Full team, funded too.
- MailSpring https://github.com/Foundry376/Mailspring/. Fork of Nylas (which is
  no longer maintained). Desktop application (only). React frontend with
  Electron. Uses C++ for its backend sync engine which is _closed source_(!).
  Very featureful. Themes. Plugins.

### Other

- Test https://www.mail-tester.com/
- NixOS Mailserver (plug n play) https://github.com/r-raymond/nixos-mailserver/wiki/A-Complete-Setup-Guide

## Linux/OS

- Oxy https://github.com/oxy-secure/oxy
- Terminal to SVG https://github.com/nbedos/termtosvg
- SSHFS. It's a FUSE filesystem over SSH.
- Kernel size tuning. https://elinux.org/Kernel_Size_Tuning_Guide#Measuring_the_kernel
- Grsecurity https://en.wikibooks.org/wiki/Grsecurity
- Cockpit shell session recording https://github.com/Scribery/cockpit/tree/scribery
- Operating Systems: Three Easy Pieces. http://pages.cs.wisc.edu/~remzi/OSTEP/
  One of many OS books that I want to find again.

### Isolation/Security

- Firejail https://firejail.wordpress.com/
- Systemd nspawn
- Graphical isolation with nspawn https://ramsdenj.com/2016/09/23/containerizing-graphical-applications-on-linux-with-systemd-nspawn.html
- Oz
