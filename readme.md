# Lists

_Pending_: This didn't scale well. Ended up using TW5 to store notes and back
them up into git-annex. Not perfect but less linear than is required here.

Repo to store lists of bookmarks, interesting projects, websites, blogs,
concepts, code, etc. Normally I save things as bookmarks or OneTab, but that
becomes harder across many devices and warrants a central storage.

Ideally I'd publish all of this on my own server in a way that's more flexible
to support search, tags, and levels of privacy. However, I don't have a live
server yet. Most lists I'd publish publicly, so those are posted here.
