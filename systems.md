Notes on the software stacks of machines:

## Haven

The server.

Here are some ideas for services:

- Caddy (over NGINX)
- Koa
- Pastebin-like service for quick filesharing
- Git Tea
- TiddlyWiki sync adaptor
- Worker node for crypto and some federated services
- Personal website. https/ipfs/onion
- NextCloud
- Fennel (DAV)
- IndieAuth or some form of OAuth2
- IRC bouncer
- Lounge, for IRC
- Cockpit
- Email stack
- Portainer
- PouchDB as a service for projects
- VPN using WireGuard
- Calendar application

Need to research:

- Ways of running and editing code sandboxes remotely
- How to remotely call for packages to be built with Spack or Portage
- How to handle backups, both offsite of the server, and as a host for clients
- Email notifications for certain events: storage quotas, logins, etc

Security:

- SSHGuard over Fail2Ban
- Snort
- NFTables

## Sugar

The Chromebook.

Need to research:

- Depthcharge, the ChromiumOS boot process, kernels+rootfs A/B/C, and
  automatic fallbacks when an autoupdate fails to boot.
- Freon. How Chrome interfaces directly with the kernel framebuffer.
- How the caps lock key can be mapped to a modifier for Home/End/Page Up+Down.
- Encrypted state partition, vs unencrypted but hash verifiable OS partition.
  - How to use the TPM to allow that to work. See Nikola Kovel's talk for an
    example with CentOS http://chaosophia.net/papers/luks_tpm_and_full_disk_encryption_without_password.html
  - Per file hash verifiability? Gentoo Wiki includes some notes on that form
    of intrusion detection.
